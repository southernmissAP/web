<?php
// TechBench dump website translation file. Fully supported in version 2.3 and newer.
// Translation information
$translation['langName']               = 'Italian';
$translation['langNameLocal']          = 'Italiano';
$translation['langCode']               = 'it-IT';
$translation['authorName']             = 'garf02';
$translation['authorLink']             = 'https://forums.mydigitallife.info/members/6748-garf02';
$translation['langCodeMs']             = 'it-it'; //used internally when connecting to Microsoft API
$translation['timeZone']               = 'Europe/Rome';

// Navigation bar
$translation['tbDump']                 = 'TechBench dump';
$translation['tbDumpDownload']         = 'TechBench download';
$translation['homePage']               = 'Home';
$translation['downloads']              = 'Download';
$translation['moreMenu']               = 'Altro';
$translation['aboutPage']              = 'Informazioni';
$translation['githubGist']             = 'Gist';
$translation['markdownFile']           = 'Markdown file';
$translation['githubRepoScript']       = 'GitHub repository (script)';
$translation['githubRepoWeb']          = 'GitHub repository (sito web)';

// Main strings
$translation['techInfo']               = 'Informazioni tecniche';
$translation['lastUpdate']             = 'Ultimo aggiornamento';
$translation['productsNumber']         = 'Numero di prodotti';
$translation['searchBar']              = 'Cerca...';
$translation['searchResults']          = 'Risultati per';
$translation['warning']                = 'Avviso';
$translation['searchNoResults']        = 'Non ci sono risultati per questa ricerca.';
$translation['prodSelect']             = 'Prodotti disponibili per questa categoria';
$translation['prodLangSelect']         = 'Lingue disponibili per questo prodotto';
$translation['noProducts']             = 'Non ci sono prodotti in questa categoria.';
$translation['linkExpireTitle']        = 'I link scadono';
$translation['linkExpire1']            = 'I link sono validi per 24 ore dalla loro creazione.';
$translation['linkExpire2']            = 'I link scadranno';
$translation['directLinksTitle']       = 'Collegamenti per i download diretti';
$translation['directLinksLine1']       = 'Hai bisogno di condividere un collegamento diretto con qualcuno? Usa i link qui sotto, per generare un link aggiornato al volo.';
$translation['linkNotChecked']         = 'Questo sito web <b>NON</b> controlla se i file esistono sui server Microsoft';
$translation['footerNotice']           = '<abbr title="TechBench dump website">TBDW</abbr> <a href="https://forums.mydigitallife.info/threads/72165">collaboratori</a>';
$translation['insiderNotice']          = 'Hai scelto di scaricare un prodotto Windows Insider. Per essere in grado di recuperare correttamente i link di download è necessario essere registrati nella <b><a href="https://www.microsoft.com/en-us/software-download/windowsinsiderpreviewadvanced">pagina Windows Insider</a></b>.';

// About page
$translation['aboutPageTitle']         = 'Informazioni su questa pagina';
$translation['aboutPageContent']       = 'Questo sito web è stato creato in modo semplice. Puoi facilmente scaricare i prodotti direttamente da Microsoft.<br>
Questo sito web o l\'autore non è in alcun modo affiliato con Microsoft Corporation.';
$translation['aboutThanksTitle']       = 'Grazie';
$translation['aboutThanksContent']     = 'Grazie a tutti quelli che vogliono contribuire a questo progetto con le traduzioni o altri mezzi.';
$translation['aboutTranslationsTitle'] = 'Traduzioni';
$translation['language']               = 'Lingue';
$translation['authors']                = 'Autori';
$translation['aboutLicenseTitle']      = 'Licenza';

// Product names
$translation['win7']                   = 'Windows 7';
$translation['win81']                  = 'Windows 8.1';
$translation['win10']                  = 'Windows 10';
$translation['win10th1']               = 'Windows 10 Threshold 1';
$translation['win10th2']               = 'Windows 10 Threshold 2';
$translation['win10rs1']               = 'Windows 10 Redstone 1';
$translation['win10rs2']               = 'Windows 10 Redstone 2';
$translation['win10rs3']               = 'Windows 10 Redstone 3';
$translation['win10rs4']               = 'Windows 10 Redstone 4';
$translation['win10rs5']               = 'Windows 10 Redstone 5';
$translation['win10rs6']               = 'Windows 10 19H1';
$translation['win10ip']                = 'Windows 10 Insider Preview';
$translation['office2007']             = 'Office 2007';
$translation['office2010']             = 'Office 2010';
$translation['office2011']             = 'Office 2011 for Mac';
$translation['allProd']                = 'Tutti i prodotti';
$translation['otherProd']              = 'Altri prodotti';

// Product descriptions
$translation['win7_desc']              = 'Nominato da molti come il migliore Windows';
$translation['win81_desc']             = 'Versione corretta di Windows 8';
$translation['win10_desc']             = 'La versione di Windows più controversa di sempre';
$translation['office2007_desc']        = 'Microsoft Office suite dell\'anno 2007';
$translation['office2010_desc']        = 'Microsoft Office suite dell\'anno 2010';
$translation['office2011_desc']        = 'Microsoft Office 2011 per Apple macOS';
$translation['allProd_desc']           = 'Visualizza tutti i prodotti di tutte le categorie in una lista';
$translation['otherProd_desc']         = 'Prodotti che non corrispondono a nessuna categoria';

// Other strings
$translation['unknownName']            = 'Nome prodotto sconosciuto';
$translation['idName']                 = 'ID';
$translation['archx64']                = '64-bit';
$translation['archx86']                = '32-bit';
$translation['downloadName']           = 'Download';
$translation['waitTitle']              = 'Attendere prego...';
$translation['waitLangText']           = 'Si prega di attendere, recupero elenco di lingue disponibili...';
$translation['waitDlText']             = 'Si prega di attendere, recupero download...';
$translation['jsRequired']             = 'Questa pagina richiede JavaScript funzionante per comunicare con i server Microsoft.';
$translation['fileReady']              = 'Il file è pronto per il download';
?>
