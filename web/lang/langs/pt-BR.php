<?php
// TechBench dump website translation file. Fully supported in version 2.3 and newer.
// Translation information
// Added - pt-BR Translation by - Ygor Almeida - www.extrememods.com.br
// 25-05-2019

$translation['langName']               = 'Portuguese-Brazil';
$translation['langNameLocal']          = 'Português (Brasil)';
$translation['langCode']               = 'pt-BR';
$translation['authorName']             = 'Ygor Almeida';
$translation['authorLink']             = 'https://gitlab.com/ygor.almeida';
$translation['langCodeMs']             = 'pt-br'; //used internally when connecting to Microsoft API
$translation['timeZone']               = 'America/Sao_Paulo';

// Navigation bar
$translation['tbDump']                 = 'TechBench dump';
$translation['tbDumpDownload']         = 'TechBench downloads';
$translation['homePage']               = 'Home';
$translation['downloads']              = 'Downloads';
$translation['moreMenu']               = 'Mais';
$translation['aboutPage']              = 'Sobre';
$translation['githubGist']             = 'Gist';
$translation['markdownFile']           = 'Markdown file';
$translation['githubRepoScript']       = 'GitHub repositório (script)';
$translation['githubRepoWeb']          = 'GitHub repositório (website)';

// Main strings
$translation['techInfo']               = 'Informação Técnica';
$translation['lastUpdate']             = 'Ultima Atualização';
$translation['productsNumber']         = 'Número de produtos'; // Number of products // double check
$translation['searchBar']              = 'Pesquisar...';
$translation['searchResults']          = 'Resultados para';
$translation['warning']                = 'Alerta';
$translation['searchNoResults']        = 'Não existem resultados para essa pesquisa.';
$translation['prodSelect']             = 'Produtos disponíveis para essa categoria';
$translation['prodLangSelect']         = 'Idiomas disponíveis para esse produto';
$translation['noProducts']             = 'Não existem produtos para esta categoria.';
$translation['linkExpireTitle']        = 'Validade dos Links';
$translation['linkExpire1']            = 'Os Links são validos por 24 horas da data de criação.';
$translation['linkExpire2']            = 'Os links irão expirar';
$translation['directLinksTitle']       = 'Download diretor dos Links';
$translation['directLinksLine1']       = 'Precisa compartilhar os links diretos com alguém? Utilize esses links abaixo, para gerar um link compartilhavel em tempo real.';
$translation['linkNotChecked']         = 'Este site <b>NÃO VERIFICA</b> se os arquivos existem nos servidores da Microsoft';
$translation['footerNotice']           = '<abbr title="TechBench dump website">TBDW</abbr> <a href="https://forums.mydigitallife.info/threads/72165">Colaboradores</a>';
$translation['insiderNotice']          = 'Você escolheu fazer Download de uma versão de produto <b>Windows Insider<b>. Para que os links de download sejam disponibilizados com sucesso você precisa estar logado <b><a href="https://www.microsoft.com/en-us/software-download/windowsinsiderpreviewadvanced">Microsoft - Windows Insider - Portal</a></b>.';

// About page
$translation['aboutPageTitle']         = 'Sobre esta página';
$translation['aboutPageContent']       = 'Este site foi criado com um propósito simples. Aqui você pode baixar os arquivos direto dos servidores Microsoft.<br>
Este Site e Seu Autor não são Membros ou Associados a Microsoft Corporation.';
$translation['aboutThanksTitle']       = 'Obrigado';
$translation['aboutThanksContent']     = 'Obrigado a todos que de alguma maneira contribuiram com o projeto ou tradução.';
$translation['aboutTranslationsTitle'] = 'Traduções';
$translation['language']               = 'Idiomas';
$translation['authors']                = 'Autores';
$translation['aboutLicenseTitle']      = 'Licença';

// Product names
$translation['win7']                   = 'Windows 7';
$translation['win81']                  = 'Windows 8.1';
$translation['win10']                  = 'Windows 10';
$translation['win10th1']               = 'Windows 10 Threshold 1';
$translation['win10th2']               = 'Windows 10 Threshold 2';
$translation['win10rs1']               = 'Windows 10 Redstone 1';
$translation['win10rs2']               = 'Windows 10 Redstone 2';
$translation['win10rs3']               = 'Windows 10 Redstone 3';
$translation['win10rs4']               = 'Windows 10 Redstone 4';
$translation['win10rs5']               = 'Windows 10 Redstone 5';
$translation['win10rs6']               = 'Windows 10 19H1';
$translation['win10ip']                = 'Windows 10 Insider Preview';
$translation['office2007']             = 'Office 2007';
$translation['office2010']             = 'Office 2010';
$translation['office2011']             = 'Office 2011 para Mac';
$translation['allProd']                = 'Todos os Produtos';
$translation['otherProd']              = 'Outros produtos';

// Product descriptions
$translation['win7_desc']              = 'Nomeado por muitos com o Melhor Windows de todos os Tempos';
$translation['win81_desc']             = 'Versão Corrigida do Windows 8';
$translation['win10_desc']             = 'O Windows mais controverso de toda a história';
$translation['office2007_desc']        = 'Microsoft Office suite do ano de 2007';
$translation['office2010_desc']        = 'Microsoft Office suite do ano de 2010';
$translation['office2011_desc']        = 'Microsoft Office 2011 para o Apple MacOS';
$translation['allProd_desc']           = 'Mostrar todos os produtos de todas as categorias em uma lista';
$translation['otherProd_desc']         = 'Os produtos não correspondem a nenhuma categoria selecionada';

// Other strings
$translation['unknownName']            = 'Nome do Produto desconhecido';
$translation['idName']                 = 'ID';
$translation['archx64']                = '64-bit';
$translation['archx86']                = '32-bit';
$translation['downloadName']           = 'Download';
$translation['waitTitle']              = 'Aguarde...';
$translation['waitLangText']           = 'Por favor Aguarde enquanto recuperamos a lista de idiomas disponíveis...';
$translation['waitDlText']             = 'Por favor Aguarde enquanto recuperamos os links para Download...';
$translation['jsRequired']             = 'Esta página requer JavaScript para se comunicar com os servidores Microsoft.';
$translation['fileReady']              = 'Arquivo pronto para realizar Download';
?>
