<?php
// TechBench dump website translation file. Fully supported in version 2.3 and newer.
// Translation information
$translation['langName']               = 'German';
$translation['langNameLocal']          = 'Deutsch';
$translation['langCode']               = 'de-DE';
$translation['authorName']             = 'wavemaster76';
$translation['authorLink']             = 'https://forums.mydigitallife.info/members/1254778';
$translation['langCodeMs']             = 'de-de'; //used internally when connecting to Microsoft API
$translation['timeZone']               = 'Europe/Berlin';

// Navigation bar
$translation['tbDump']                 = 'TechBench dump';
$translation['tbDumpDownload']         = 'TechBench Downloads';
$translation['homePage']               = 'Home';
$translation['downloads']              = 'Downloads';
$translation['moreMenu']               = 'Mehr';
$translation['aboutPage']              = 'Über';
$translation['githubGist']             = 'Gist';
$translation['markdownFile']           = 'Markdown-Datei';
$translation['githubRepoScript']       = 'GitHub Repository - Skript';
$translation['githubRepoWeb']          = 'GitHub Repository - Webseite';

// Main strings
$translation['techInfo']               = 'Technische Informationen';
$translation['lastUpdate']             = 'Letzte Aktualisierung';
$translation['productsNumber']         = 'Anzahl Produkte';
$translation['searchBar']              = 'Suchen...';
$translation['searchResults']          = 'Treffer für';
$translation['warning']                = 'Warnung';
$translation['searchNoResults']        = 'Keine Ergebnisse für deine Suchanfrage.';
$translation['prodSelect']             = 'Verfügbare Produkte für diese Kategorie';
$translation['prodLangSelect']         = 'Verfügbare Sprachen für dieses Produkt';
$translation['noProducts']             = 'Keine Produkte in dieser Kategorie.';
$translation['linkExpireTitle']        = 'Gültigkeitsdauer der Download-Links';
$translation['linkExpire1']            = 'Der Download-Link verfällt 24 Stunden nach der Erstellung.';
$translation['linkExpire2']            = 'Der Download-Link verfällt in';
$translation['directLinksTitle']       = 'Direkte Download-Links';
$translation['directLinksLine1']       = 'Du willst den Download-Link mit jemand anderem teilen? Benutze diese Links, die automatisch frische Download-Links erstellen..';
$translation['linkNotChecked']         = 'Diese Webseite kontrolliert <b>nicht</b>, ob die Datei auf den Microsoft-Servern existiert.';
$translation['footerNotice']           = '<abbr title="TechBench dump-Webseite">TBDW</abbr> <a href="https://forums.mydigitallife.info/threads/72165">Mitwirkende</a>';
$translation['insiderNotice']          = 'Du hast ein Windows Insider-Produkt ausgewählt. Damit Download-Links von diesem Produkt generiert werden können, musst du auf der <b><a href="https://www.microsoft.com/en-us/software-download/windowsinsiderpreviewadvanced">Windows Insider-Webseite</a></b> angemeldet sein.';

// About page
$translation['aboutPageTitle']         = 'Über diese Webseite';
$translation['aboutPageContent']       = 'Diese Webseite wurde mit Einfachheit im Hinterkopf erstellt. Hier kannst du einfach Produkte direkt von den Microsoft-Servern herunterladen.<br>
Diese Webseite oder sein Autor ist nicht an die Microsoft Corporation angegliedert.';
$translation['aboutThanksTitle']       = 'Danke';
$translation['aboutThanksContent']     = 'Vielen Dank an alle, die durch Übersetzungen bei diesem Projekt mitewirkt haben.';
$translation['aboutTranslationsTitle'] = 'Übersetzungen';
$translation['language']               = 'Sprache';
$translation['authors']                = 'Autoren';
$translation['aboutLicenseTitle']      = 'Lizenz';

// Product names
$translation['win7']                   = 'Windows 7';
$translation['win81']                  = 'Windows 8.1';
$translation['win10']                  = 'Windows 10';
$translation['win10th1']               = 'Windows 10 Threshold 1';
$translation['win10th2']               = 'Windows 10 Threshold 2';
$translation['win10rs1']               = 'Windows 10 Redstone 1';
$translation['win10rs2']               = 'Windows 10 Redstone 2';
$translation['win10rs3']               = 'Windows 10 Redstone 3';
$translation['win10rs4']               = 'Windows 10 Redstone 4';
$translation['win10rs5']               = 'Windows 10 Redstone 5';
$translation['win10rs6']               = 'Windows 10 19H1';
$translation['win10_19h2']             = 'Windows 10 19H2';
$translation['win10ip']                = 'Windows 10 Insider Preview';
$translation['office2007']             = 'Office 2007';
$translation['office2010']             = 'Office 2010';
$translation['office2011']             = 'Office 2011 für Mac';
$translation['allProd']                = 'Alle Produkte';
$translation['otherProd']              = 'Andere Produkte';

// Product descriptions
$translation['win7_desc']              = 'Von vielen das beste Windows genannt';
$translation['win81_desc']             = 'Stabile Version von Windows 8';
$translation['win10_desc']             = 'Das kontroverseste Windows überhaupt';
$translation['office2007_desc']        = 'Microsoft Office-Suite von 2007';
$translation['office2010_desc']        = 'Microsoft Office-Suite von 2010';
$translation['office2011_desc']        = 'Microsoft Office 2011 für Apple macOS';
$translation['allProd_desc']           = 'Show all products from all categories in one list';
$translation['otherProd_desc']         = 'Products that do not match any category above';

// Other strings
$translation['unknownName']            = 'Unbekannter Produkt-Name';
$translation['idName']                 = 'ID';
$translation['archx64']                = '64-bit';
$translation['archx86']                = '32-bit';
$translation['downloadName']           = 'Download';
$translation['waitTitle']              = 'Bitte warten...';
$translation['waitLangText']           = 'Bitte warten, während wir eine Liste der verfügbaren Sprachen erhalten...';
$translation['waitDlText']             = 'Bitte warten, während wir die Downloads erhalten...';
$translation['jsRequired']             = 'Diese Webseite benutzt JavaScript, um mit den Microsoft-Servern zu kommunizieren.';
$translation['fileReady']              = 'Datei ist bereit zum herunterladen';
?>
